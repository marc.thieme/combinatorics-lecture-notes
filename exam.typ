#import "lib.typ": *

= Exam preparation

==
#example[Intersecting families][
  If $A subset.eq 2^[n]$ splits $[n]$ ($forall_(, j in [n]) exists_(A in cal(A)) i in A, j in.not A$), then $
    forall_(i, j in [n]) exists_(A, B in cal(A)) : i in A, j in.not A and j in B, i in.not B
  $
]

== Remember
@remember-power-series
@remember-binom

== TODO
Primes and Moebius Inversion 

Partial Fraction Decomposition

Linear Recursions

== List of proofs
#context {
  show figure: set block(breakable: true)
  query(figure.where(kind: important-proof-kind)).intersperse(pagebreak()).sum()
}
