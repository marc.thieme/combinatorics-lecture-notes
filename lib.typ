#import "@local/theorems:0.0.1": *

#let (
  slim,
  theorem,
  lemma,
  definition,
  example,
  principle,
  notation,
  interpretation,
  algorithm,
  todo,
  advice,
) = init-theorems(
  "theorems",
  theorem: ("Satz", red.lighten(40%)),
  lemma: ("Lemma", blue),
  definition: ("Definition", green.lighten(60%)),
  example: ("Beispiel", gray),
  principle: ("Principle",),
  notation: ("Notation",),
  interpretation: ("Interpretation",),
  algorithm: ("Algorithm",),
  todo: ("TODO", yellow),
  advice: ("Advice",),
)

#let inplace-ref(ref) = {
  if ref.func() == figure and ref.kind == "theorems" {
    ref
  } else {
    ref.supplement
  }
}

#let important-proof-kind = "proof-to-remember"
#let proof-to-remember(body) = {
  show figure: set block(breakable: true)
  figure(kind: important-proof-kind, supplement: none, {
    set align(start)
    body
  })
}

#let square = box(square(width: 0.5em))
#let circle = box(circle(width: 0.5em))
#let triangle = text(0.8em)[*#math.triangle*]
#let sep = h(0.3em) + line(length: 0.9em, angle: 90deg) + h(0.2em)
#let arrup = $arrow.t$
#let arrright = $arrow.r$

#let comb((n,), ks) = math.binom(n, ks.join($,space$))
#let intsect = $limits(sect.big)$
#let unio = $limits(union.big)$
