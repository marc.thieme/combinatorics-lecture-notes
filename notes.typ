#import "lib.typ": *

#show: project.with("COMBINATORICS", "Prof. Axenovich", "Marc Thieme")
#set text(12pt)
// Don't span equations over two lines
#show math.equation: box

#set ref(supplement: inplace-ref)

= Basics
== Introduction
Fields:
- existence
- enumeration
- classification
- optimizatoin
- interconnections

== Counting
#principle[Basic Counting Principles][
  / Addition: $S = S_1 union S_2,, S_1 union S_2 = nothing => |S| = |S_1| + |S_2|$
  / Multiplication: $S = S_1 times S_2 quad S_1 times S_2 := lr({(s_1, s_2) : s_1 in S_1, s_2 in S_2})$
  / Subtraction: $S = T without T_1, quad T_1 subset.eq T => |S| = |T| - |T_1|$
  / Bijection: For finite sets $S$ and $T$: $|S| = |T| <=> exists$ bijection between $S$ and $T$
]

#principle[Pigeonhole Principle][
  If $S_1, ..., S_m$ are pairwise dijoint sets:$
    |S_1| + ... + |S_m| = n quad "then" quad &exists_(i in {1, ..., m}) |S_i| >= ceil(n/m) \
    "and" quad                               &exists_(j in {1, ..., m}) |S_j| <= floor(n/m)
  $
]
#principle[Double Counting][
  Let $S subset.eq A times B$ then $
    |S| &= sum_(a in A) |{b in B : (a, b) in S}| \
        &= sum_(b in B) |{a in A : (a, b) in S}|
  $
]

== Ordered Arrangements
#definition[Ordered Arrangement][
  An *Ordered Arrangement* is a map $S : [n] -> X$
]
*Synonyms:* _Function, String, Word, Tuple, Sequence_

#definition[Arr. Equivalence][
  Ordered Arrangements $S_1, S_2 : [n] -> X$ are equivalent if $
    exists_("shift" sigma) : S_1(i) = S_2(i+sigma)
  $ with addition $mod n$
]

#definition[Circular Arrangement][
  // TODO
]

== Others
#definition[Permutation][
  A bijective map $pi : [n] -> X$
]
#definition[k-Permutation][
  An injective map $pi : [k] -> X$
]
#definition[k-Combination][
  - For normal sets: Synonym for k-Subset
  - For multisets: A k-element multiset which contains the types of the multiset and
    of each type less than the original multiset
]
#definition[Circular k-permutation][
  An equivalence class of a $k$-permutation where the following implication holds: $
    i + s eq.triple j quad (mod k) => pi_1 (i) = pi_2 (j)
  $
]
#notation[Permutation groups][
  / Symmetric Group $S_n$: A set of all permutations is called a symmetric group
  / $P(n, k)$: Set of all $k$-permutations for an $|X| = n$
]

#theorem(
  [Size of permutation groups],
  {
    h(1fr)
    $|P(n, k)| = n! / (n-k)!$
    h(1fr)
    $|P_c (n, k)| = n! / (k(n-k)!)$
    h(1fr)
  },
)

== Multinomial Coefficients
#definition[General definition of binomial coefficient][
  $ binom(alpha, k) = (alpha (alpha - 1) (alpha - 2) ... (alpha - k + 1)) / k! , quad alpha in RR, k in NN $
]
#theorem[Identities of binom coeff.][
#let su = $sum_(k = 0)^n$
+ $ su binom(n, k) = 2^n $
+ $ su binom(n, k)^2 = binom(2n, n) $
+ $ su binom(k, m) = binom(n + 1, m + 1) $
+ $ su (-1)^k binom(n, k) = 0 $
+ $ su binom(m + k - 1, k) = binom(n + m, n) $
+ $ su binom(alpha, k) binom(beta, n-k) = binom(alpha + beta, n) $
+ $ binom(1/2, n) = 1/n (-1)^(n + 1) binom(2n, n) 2^(1-2n) $
] <remember-binom>

#definition[Multinomial Coefficient][$
    comb(n; k_1, ..., k_r) = n! / (k_1! dot ... dot k_r!)\
    = binom(n, k_1) binom(n - k_1, k_2) ... binom(n - k_1 - ... - k_(r-1), k_r)
  $]
#theorem[Pascal's][$
    comb(n; k_1, ..., k_r) = sum_(i=1)^r comb(n-1; k_1, ..., k_i-1, ..., k_r)
  $]
#lemma[
  $binom(n, k) = binom(n - 1, k - 1) + k dot binom(n - 1, k)$
]

#interpretation[Multinomial Coefficient][
  + How many ways are there to put $n$ labelled objects into $r$ bins such that the
    $i$th bin contains $k_i$ objects.
  + The coefficients of expanding the $n$th power of an $r$-variable sum.
  + The number of permutations of a multiset.
]
#theorem[Permutations of a multiset][
  The number of permutations of a multiset with repitition numbers $k_1, ..., k_r$
  is $comb(n; r_1, ..., r_k)$ where $n = r_1 + ... + r_k$
]
#proof[
  We imagine for each $i$th type, we have a bin of size $k_i$.
  Now, we see a bijection from a permutation of the multiset to a placement of
  $[n]$ into the bins. The latter has order $comb(n; r_1, ..., r_k)$
]

#theorem[$s$-combinations of a multiset][
  The number of $s$-combinations of a multiset with large (!) repitition numbers $k_1, ..., k_r >= s$
  is $binom(r + s - 1, s)$
]
#proof[
  We look at a permutation of the multiset ${s dot circle, r dot sep}$.
  We can biject this to a unique $s$-combination by counting the $circle$
  until the first $sep$ and adding just as many elements of the first type.
  We continue this until the end of the permutation.

  We can count the permutations of ${s dot circle, (r-1) dot sep}$ by $
    comb(r + s - 1; r-1, s) = binom(r - 1, s)
$
]

= Twelvefold way
#let stirling(k, n) = $s^(I I)_#k (#n)$
#image("res/twelvefold-way.png")
#image("res/twelvefold-interpretation.png")

== Examples
#example[Monotone Path][
  #(definition)[
    A monotone path is a sequence over {#arrright, #arrup} whose steps end in some $(m, n)$
  ]
  #theorem[
    There are $binom(m + n, m)$ such paths.
  ]
]
#example[Legal monotone paths][
  For this exmample, an illegal path is a path crossing the diagonal.
  We search for the number of paths of size $n$. We will use $k in [n/2]_0$ to denote
  We first calculate the number of illegal paths ending in $(n - k, k) =: C$.

  First observe that, legal and illegal combined, there are $binom(n, k)$ monotone paths to $C$.
  Second, we count the illegal paths to $C$.
  For that, observe the bijection of those illegal paths to all monotone paths to $(k - 1, n - k + 1) := Y$ described in the following:

  Take any illegal path to $P$. Let $x$ be the x-coordinate of the first move over the diagonal and $y$ the y-coordinate after the move.
  Obtain a new path $P'$ with the start of $P$, but all steps after $x$ mirrored.
  We see that $P$ contained, after $x$, $n - k - x =: x'$ right-steps and $k - y =: y'$ up-steps.
  After mirroring, these numbers switch.

  We also now that the path crossed the diagonal at $x$ and therefore, $y = x + 1$.

  As such, $ "end"(P') &= (x + y', y + x')
  \ &= (x + (k - y), y + (n - k - x)) 
  \ &= (x + k - (x + 1), (x + 1) + n - k - x)
  \ &= (k - 1, n - k + 1) =: C'$

  Since $C'$ is over the diagonal of illegality, any monotone path ending in $C'$ must still cross this diagonal.

  Therefore, for all monotone paths, we can reverse the operation again by identifying the
  coordinates where the path crosses the diagonal and mirror all subsequent steps again, resulting in an illegal path ending in $C$.

  As such, we have a bijection between the two classes.
  Since we know that there are exactly $binom(k - 1 + n - k + 1, k - 1) = binom(n, k - 1)$ paths ending in $C'$,
  we know know that there are also $binom(n, k-1)$ illegal paths ending in $C$.

  Therefore, we conclude that there are $binom(n, k) - binom(n, k - 1)$ legal paths ending in $C$.
]
#example[Cake Cutting][
  #definition[
    The _cake number_ is the maximal number of connected components of $RR^m without union H_i$ where $H_i$ are affine hyperplanes.
  ]
  #theorem[
    $ c(m, n) = sum^m_(i=0) binom(n, i) $
  ]]
#theorem[Identities][
  - $sum_(k=0)^n (-1)^k binom(n, k) = 0$
  Picture:
  - $sum_(k in [n]) k = binom(n+1, 2)$
  - $sum_(k in [n]) (2k - 1) = n^2$
  Double Counting:
  - $sum_(k = 0)^n 2^k binom(n, k) = 3^n$
  - $sum_(i = 0)^m binom(n+i, i) = binom(n + m + 1, m)$ // NOTE: Look at proof
  Analysis:
  - $(x + 1)^n = sum^n_(i=0) binom(n, i) x^i$
  - $n dot 2^(n-1) = sum^n_(i=0) i binom(n, i)$
  - $n(n+1)2^(n-2) = sum^n_(i=0) i^2 binom(n, i)$
]

= Permutations
#definition[De Bruijn Sequence][
  Is the circular binary sequence that contains all binary strings of length $k$ as intervals.
]
#theorem[De Bruijn Sequence][
  Has length $2^k$
]

#stack(
  dir: ltr,
  box(
    width: 60%,
    theorem[Recall][Permutation Decomposition][
      Any permutation is
      - a decomposition of "disjoint" cycles
      - The product of transpositions
    ],
  ),
  align(bottom + center, image("res/permutation-cycle.png", width: 30%)),
)

#definition[Permutation #math.pi][Inversion][
  A pair $(i, j)$ is an _inversion_ in $pi$ if
  $ i < j and pi^(-1) (i) > pi^(-1) (j) $
]
#definition[Permutation #math.pi][Inversion Sequence][
  Define $alpha_i := |{j in [n] | (i, j) "is inversion"}|$. \
  The *inversion sequence* of $pi$ is the sequence $alpha_1 alpha_2 ...$.
]

#theorem[Any permutation can be recovered from its inversion sequence.]

#definition[Permutation][Order][
  The smallest $k$ such that $pi^k = id$
]

#theorem[Permutation #math.pi][Cycles][Order][
  The order $k$ of $pi$ is the smallest common multiple of the lengths of its cycles.
]

#definition[Permutation #math.pi][Discriminant][
  $N(pi) = n - \#C$ where $\#C$ is the number of disjoint cycles in the cycle decomposition of $pi$.
]
#theorem[Transposition parity][
  If $pi$ is the product of $k$ transpositions, then $N(pi)$ and $k$ have the same parity.
]

== Derangements
#definition[Derangement][
  A permutation is a derangement if it is fixpoint-free
  #notation[
    $D_n$ denotes the set of all derangements of $[n]$.
  ]
]

#theorem[
  There are $|D_n| = sum_(i = 0)^n (-1)^i binom(n, i) (n - 1)!$ derangements.
]
#proof[
  Define $X_i := {"All Permutations with a fixpiont at i"}$.
  Observe $intsect_(i in I) X_i = (n - i)!$

  We formulate $
    |D_n| &= n! - sum_(I in 2^([n]) without emptyset) (-1)^(|I| + 1) intsect_(i in I) X_i 
    = n! - sum_(i = 0)^n (-1)^(i + 1) binom(n, i) (n - i)!
  \ &= sum_(i = 0)^n (-1)^i binom(n, i) (n - i)!
  $
]

#theorem[Derangement equivalences][
  $
    |D_n| &= \
    &= (n - 1)(|D_(n-1)| + |D_(n-2)|), "for" n>= 2\
    &= n |D_(n-1)| + (-1)^n\
    &= n! - sum^n_(k=0) binom(n, k) |D_k|\
    &= sum^n_(k=0) binom(n, k) (-1)^(n+k)k! = n! sum^n_(k=0) (-1)^k / k!\
    &= floor(n!/e + 1/w)\
    &tilde sqrt(2pi n) n^n / e^(n+1)\
    sum_(n>=0) |D_n| / n! x^n &= e^(-x) / (1 - x) quad forall_(x in RR without {
      1
    }) \
    &= sum_(i=0)^n (-1)^i binom(n, i) (n - i)!
  $
]

= Inclusion & Exclusion Principle
#proof-to-remember[
#principle[!][Inclusion Exclusion Principle][
  Let $A_1, ..., A_m, quad m >= 1$ be finite sets. Then
  $
    |union_(i=1)| = sum_(I subset.eq [m], I != emptyset) (
      -1
    )^(|I| + 1) |sect_(i in I) A_i|
  $
]
#proof[
  #let sec = $limits(sect.big)_(i in I) A_i$
  #let index = $I in 2^[m] without emptyset$
  #let pow = $(-1)^(|I| + 1)$
  #let un = $x in limits(union.big)_(i in [m]) A_i$
  First define $T(x) := {i : x in A_i}$. Note that $x in sec <=> I subset.eq T(x)$

  $
    &sum_(I in 2^[m] without emptyset) pow |sec| = sum_index pow sum_(x in sec) 1 =
    sum_index sum_(x in sec) pow = sum_(index\ x in sec) pow \
    = &sum_(un \ I subset.eq T(x)) pow = sum_un sum_(i = 1)^(|T(x)|) (
      -1
    )^(i + 1) binom(|T(x)|, i)
    = sum_un sum_(i = 1)^(|T(x)|) (-1)^(i + 1) binom(|T(x)|, i) \
    = &sum_un - (sum_(i = 0)^(|T(x)|) (-1)^i binom(|T(x)|, i) - 1)
    = sum_un 1 = lr(|limits(union.big)_(i in [m]) A_i|)
  $
]
]

#theorem[\# surjections][
  There are $
    sum_(i=0)^n (-1)^i binom(n, i) (n - i)^k
  $
  surjections from $[k]$ to $[n]$
]
#proof[
  We search the set $S$ of surjections from $[k]$ to $[n]$.
  Define $X_i = {"All functions which do not map to i"}$. Observe $|intsect_(i in I) X_i| = (n-|i|)^k$

  Now we can formulate: $
    |S| &= |F| - |X| = |union_(i in [n]) X_i| = sum_(I in 2^[n] without emptyset) (-1)^(I + 1) intsect_(i in I) X_i \
    &= sum_(i = 1)^n (-1)^(n) binom(n, i) (n-i)^k = n^k - sum_(i = 1)^n (-1)^(n) binom(n, i) (n-i)^k =
     sum_(i=0)^n (-1)^i binom(n, i) (n - i)^k
  $
]

#definition[
  Let $Q(n, r)$ be the circular sequence of $2n - r$ 0s and $r$ 1s s.t. no two 1s are next to each other.
]
#lemma[$Q(n, r)$][$r$ non-adjacent ones][
  $|Q|(n, r) = (2n) / (2n - r) binom(2n - 4, r)$
]

#theorem[Stronger PIE][
  Let $f, g : 2^[n] -> RR$.
  Let $ g(A) = sum_(S subset.eq A) f(S) $

  Then $ f(A) = sum_(S subset.eq A) (-1)^(|A| - |A|) g(S) quad "for" A subset.eq [n]$
]

#notation[Prime factorisation][
  $M(n)$ is the multiset of prime factors of $n$.
  #zb $M(360) = {3 dot 2, 2 dot 3, 1 dot 5}$
]

#definition(math.phi)[Euler's function][
  $phi(n) = hash{k in NN | 1 <= k <= n, gcd(n, k) = 1}$
]

#theorem[][
  For distinct primes $p_i$ let $n = p_1^a_1 ... p_k^a_k$
  $
    phi(n) = n product_(i=1)^k (1 - 1 / p_i)
  $]

#theorem[$ n = sum_(d divides n) phi(d) $]

#definition(math.mu)[Moebius Function][
  $mu(d) = cases(1 "if" d "is the product of an even number of distinct primes",
  -1 "if" d "is the product of an odd number of distinct primes",
  0 "otherwise")$
]

#lemma[
  $sum_(d divides n) mu(d) = cases(1 "if" n = 1, 0 "else")$
]

#theorem[!][Moebius Inversion][
  Let $f, g : NN -> RR$ be functions satisfying $g(n) = sum_(d divides n) f(d) $. Then:
  $ f(n) = sum_(d divides n) mu(d) g(n / d) $
]

#example[Circular Sequences of 0's and 1's]

#include "generating.typ"
#include "partitions.typ"
#include "posets.typ"
#include "intersecting.typ"
#include "designs.typ"
#include "exam.typ"
