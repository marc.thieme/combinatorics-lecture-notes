#import "lib.typ": *

#let Bo = $cal(B)$

= Designs
#definition[$t-(v, k, lambda)$ Design][
  A $t-(v, k, lambda)$ Design is a multiset $Bo$ over the ground set $V$ of size $v$ such that
  each element $B in Bo$ has size $k$ and each set of points $T in binom(V, t)$ is covered by exactly $lambda$ blocks $B in Bo$.
]

#proof-to-remember[
#theorem[Design Non-existance][
  For any $t-(v, k, lambda)$ Design $Bo$, for the number of blocks $
    |Bo| = lambda binom(v, t) / binom(k, t)
  $

  Fix any set $I subset.eq V$. Let $r_i$ be the number of blocks in $Bo$ containing $I$. Then $
    |r_i| = lambda binom(v - i, t - i) / binom(k - i, t - i)
  $
]
#proof[
  Proof of the second statement:

  We double count the set $
    S := {(T, B) | B in Bo, I subset.eq T subset.eq Bo, |Bo| = t}
  $
  *First Way:* For each set $T' in binom(V - I, t - i)$, we get a set $T = T' union I$. 
  Since each of them must be contained in $lambda$ blocks per definition of Designs, we get
  $ S = sum_(T' in binom(V - I, t - i)) lambda = binom(v - i, t - i) lambda$.

  *Second Way:* For each of the $r_i$ blocks $B in Bo$ containing $I$, there exist $binom(k - i, t - i)$ elements we can
  add to $B$ in order to obtain a set $I subset.eq T subset.eq B$. Therefore
  $ S = sum_B binom(k - i, t - i) = r_i binom(k - i, t - i)$
  And combining the equations, we obtain $r_i = lambda binom(v - i, t - i) / binom(k - i, t - i)$

  By setting $I := emptyset$, we see that the second equation from the theorem collapses into the first.
]
]
