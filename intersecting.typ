#import "lib.typ": *

= Intersecting Families
#definition[Intersecting Family][
  $X subset.eq 2^([n])$ is an *intersecting family* iff $forall_(A, B in X) A sect B != emptyset$
]

#theorem[][
  If $x subset.eq 2^([n])$ is an _intersecting family_, then $|X| <= 2^(n-1)$
]

#proof-to-remember[
#theorem[][
  Let $k, n in ZZ$ with $1 < k < n/2$

  The largest size of an _intersecting family_ of $k$-element
  subsets of $[n]$ is $binom(n-1, k-1)$.

  This family is ${A in binom([n], k)} : i in A}$ for some $i in [n]$
]
#proof[
#let Pc = $P_c$
  We look at the set of Permutations cyclic permutations $P_c$.
  We represent each $p in P_c$ by the normal permutation $p' in P$ which is cyclic equivalent to $p$ _and_ has 1 and index 1.
  Aka. we shift the cyclic permutation such that 1 is at index 1.
  Therefore, we notate such that forall $pi in P_c$, $pi(1) = 1$

  Let $F subset.eq binom([n], k)$ the largest such intersecting family.

  We say a set $S$ meets a permutation $pi in P_c$ in the window $W:=[i .. i + |S|] quad$ iff $quad unio_(j in W) pi(j) = S$ (with cyclic indexing)

  #lemma[][
    Each permutation $pi in P_c$ meets at most $k$ permutations in $F$.
  ]
  #proof[
    Take any permutation $pi in P_c$ and let $G subset.eq F$ be the family met my $pi$ in any window.
    For any $g in G$ and let $W_g$ be the window in which $g$ meets $pi$. 
    Take any $g in G$.

    Since the window of all other other elements in $G$ must also intersect $W_g$, they can cover in total not more
    than $k$ indices to the right and left of $W_g$. 

    Assume, they could and we could take the window $W_r$ which ends most far away of $W_g$ to the right and $W_l$ which ends
    most far away to the left and have them still intersect.
    The distance between both of them would be the $k$ indices of $W_g$ plus the $k+1$ indices not in $W_g$.
    But since $W_l$ and $W_r$ together can only cover $2k$ elements, there exists one residual element separating the two.
    This means that they would have to intersect over the other direction.
    However, since they are both $k < n/2$ long, this implies that one of them does not intersect with $W_g$ anymore, which
    is also impossible.

    Therefore, we can map at most $k$ elements in sum to the right and left of $W_g$ to elements in $G$.
    This bounds $|G| <= k$
  ]

  In the converse direction, each set $f in F$ maps to $k! (n-k)!$ permutations meeting it.
  We see this by fixing the $k$ elements of $f$ at the start of a non-cyclic permutation and having $k!$ permutations
  of this prefix, and equivalently fixing the other $n-k$ elements at the end of this permutation and having $(n-k)!$
  permutations of this suffix. 
  This equates to the number of cyclic permutations because we can biject them by rotating the 1 at index 1 and
  such having a canonical representation.
  Also, no two $f_1, f_2 in F$ map to the same permutation because j

  With this in mind, we can formulate see $
    N = sum_(f in F) "# permutations meeting" f &= |F| k! (n-k)! \
    N = sum_(p in P_c) \# f in F "met by" p &<= k |P_c| = k (n-1)!
  $
  And therefore $
    |F| k! (n-k)! = N <= k (n-1)! \
    <=> |F| <= (k (n-1)!)/(k! (n-k)!) = ((n-1)!)/((k-1)! ((n-1)-(k-1))!) = binom(n-1, k-1)
  $

  Now assume there exists any intersecting family $F in binom([n], k)$.
  In this case, the inequality from before must be an equality.
  Therefore, each permutation $pi in P_c$ meets exactly $k$ elements in $F$.

  In the previous proof, we saw for a permutation $pi in F$ and the set $G$ of $k$ sets met,
  there exists some element $x in intsect(G)$. 

  Now take another permutation $rho in F$ and the set $H$ of $k$ sets met.
  Let $y in intsect(H)$.

  Assume $x != y$. Let w.l.o.g. $x < y$. Take the left-most window of the elements 
]
]

