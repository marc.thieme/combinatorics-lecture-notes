#import "lib.typ": *

= Generating Functions
== Partial Fraction Decomposition
#principle[Partial Fraction Decomposition][
  
]

== Basics
#definition[Ordinary Generating Function][
  Given a sequence $a_0, a_1, ...$, the *ordinary generating function* for
  $(a_n)_(n in {0, 1 ,...})$ is the formal power series
  $g(x) = a_0 + a_1 x + x_2 x^2 + ... = sum_(n = 0)^infinity a_n x^n$
]

#theorem[Convergence of common series'][
  + $ sum_(n = 0)^infinity x^n = 1 / (1 - x) = 1 + x + x^2 + x^3 + ... $
  + $ sum_(n = 0)^infinity (-1)^n x^n = 1 / (1+x) = 1 - x + x^2 - x^3 + x^4 - ... $
  + $ sum_(n = 0)^infinity (n + 1) x^n = 1 / (1-x)^2 = (1/(1 - 2))' = 1 + 2x + 3x^2 + 4x^2 + ... $
  + $ x / (1-x^2) = x + x^3 + x^5 + ... = x (1 + x^2 + x^4 + ...) $
  + $ sum_(n = 0)^infinity binom(alpha, n) x^k = (1 + x)^alpha $
] <remember-power-series>

== Lecture 8
#lemma[Partial Fraction Decomposition][
  , p,q polynomials with $deg(p) < deg(q)$.\
  $p(x) / q(x)$
]

=== Counting with generating functions
Given set A with weights assigned to the elements (weights $in NN$), $a_n:= hash "elements of A with weight n"$.

We say that the generating function $g(x)$ for $(a_n)$ "counts" A with respect tot he weights, denote it $g_A (x)$

We have the following
#let then = $quad => quad$
#principle[Operations][
  Let $A, B, C$ disjoint sets.
  / Union: $C := A union.dot B then g_C (x) = g_A (x) + g_B (x)$
  / Cartesian Product: $C := A times B$ where $"weight"((s, t)) = "weight"(s) + "weight"(t)$
    $then g_C (x) = g_A (x) dot g_B (x)$
  / Substitution: Let $A$ be a set of sets. Let $C := union {a times {b} | a in A, b in B}$
    $then g_C = g_A (g_B (x))$
  / Pointing: Let $A$ be a set of sets. Let $B$ be a set of "members of A with distinguished elements".
  $ g_A (x) = sum_(n = 0)^infinity a_n x^n, quad g_B (x) = sum_(n = 0)^infinity n a_n x$.
  $then g_B (x) = x d / (d x) g_A (x)$
]

#example[Substitution][
  Let A be the set of sequences over ${a}$, $B = {0, 11}$, weights the length of words.

  C-substitution of $B$ onto $A$, i.e. set of words over ${a_0, a_11}$.
  #zb $a a a arrow.squiggly a_0 a_11 a_0$.
  #then $g_C (x) = g_A (g_B (x)))$
]

#lemma[Cauchy Sum][
  // TODO: Is this correct?
  $
    sum_(n = 0)^infinity a_n x^n dot sum_(n = 0)^infinity b_n x^n = sum_(n = 0)^infinity sum_(i = 0)^n (
      a_n dot b_(n - i)x^n
    )
  $
]

== Exponential Generative Functions
Given $a_0, a_1, ...$, we let $g_exp (x) = sum_(n = 0)^infinity a_n x^n / n!$

#theorem[Product Rule][
  Applies when for each combination, you can intersperse the weight values// todo: $"weight"((s, t)) = "weight"(s) $
  $sum_(n = 0)^infinity a_n x^n / n! sum_(n = 0)^infinity b_n x^n / n!
    &= sum_(n = 0)^infinity sum_(m = 0)^infinity (a_m x^m) / m! dot (b_(n-m) x^(n-m)) / (
      n-m
    )! \
    &= sum_(n = 0)^infinity (
      sum_(m = 0)^infinity binom(n, m) a_m b_(n-m)
    )x^n / n!$
]

#theorem[Shift Rule][
  Let $f(x) = sum_(n = 0)^infinity a_n x^n/n!$. Then $
    f'(x) = sum_(n = 1)^infinity a_n x^(n-1) / (n-1)! = sum_(n = 0)^infinity a_(n+1) x^n/n!
  $
]



== (Linear) Recursions
#definition[!][Recursion][
  A sequence $a_0, a_1, ...$ is a *recursion* if there exists a function $f$ and a $k in NN$ such that $
    a_n = f(a_(n-1), ..., a_(n-k))
  $
]

#definition[For some inital conditions][Solution][
  A solution of a recursion is a the sequence $(a_n)_(n in NN)$ for some initial
  values if it upholds the recurrence relation.
]

#definition[Linear Recursion][$
    a_n = c_1 (n) a_(n-1) + c_2 (n) a_(n-1) + ... + c_k (n) a_(n-k) + g(n)
  $]

#definition[Linear Homogonous Recursion][
  / Linear Homogonous: if $g eq.triple 0$
  / Linear non-homogonous: Else
]

=== Linear Homogenous Recurrence

#theorem[Linearlity of Solutions][
  Let $(b_n)_(n in NN), (d_n)_(n in NN)$ be solutions of $f$.
  Then $beta b_n + delta d_n$ is also a solution.
]
#proof[
  Since $f(a_(n-1), ..., a_(n-k)) = c_1 a_(n-1) + ... + c_k a_(n-k)$, we know $
    b_n = c_1 b_(n-1) + ... + c_k b_(n-k)\
    d_n = c_1 d_(n-1) + ... + c_k d_(n-k)
  $
  and we can multiply and add them so we get $
    (beta b_n + delta d_n)_n = c_1 (beta b_(n-1) + delta d_(n-1)) + ... + c_k (beta b_(n-k) + delta d_(n-k))
  $
]

#definition[With $a_n = x^n$][Characteristic Polynomial][
  $
    &a^n - c_1 a_(n-1) - c_2 a_(n-2) - ... - c_k a_(n-k) &= 0 \
    --> &x^k - c_1 x^(k-1) - c_2 x^(k-2) - ... - c_k &= 0 <--
  $
]

#theorem[
  If $q$ is a root of the characteristic polynomial with multiplicity $i$, then
  $(n^(1..i) q^n)_(n in NN)$ are solutions of the recursion
]

#algorithm[
  We look for a solution of the form $a_n = x^n$

  #set math.equation(numbering: "(1)")
  #set enum(numbering: "STEP 1:")
  Let $ a_n = c_1 a_(n-1) + ... + c_k a_(n-k) $ <def>
  + $a_n := x^n$
  + Plug in @def. $
    x^n = c_1 x^(n-1) + c_2 x^(n-2) + ... + c_k x^(n - k) quad | \/ x^(n-k)\
    x^k = c_1 x^(k-1) + c_2 x^(k-2) + ... + c_k\
    x^k - c_1 x^(k-1) - ... c_k = 0
  $
  + Find roots $q_i$ of the char. polyn. above.
  + Solve the system of linear eqations to derive $gamma_(i, j)$
  // TODO Look up the LGS
  + Write down the explicit solution $ a_n = sum^r_(i=1) sum^(s_(i-1))_(j=0) gamma_(i, j) n^j q_i^n $
]

#advice[IN THE EXAM, SOLVE LIN. HOM. RECURSIONS USING THIS CHAR. POL. ALGORITHM AND NOT USING GEN. FUNCTION]

== Non Homogonous Recursions
Recall
#definition[
  $a_n = c_1 a_(n-1) + c_2 a_(n-1) + ... + c_k a_(n-k) + g(n)$
] <non-hom>

#advice[
  Approach each summand $s(n)$ of $g(n)$ individually.
  Search one particular solution for
  $ c_1 a_(n-1) + c_2 a_(n-1) + ... + c_k a_(n-k) + s(n) $
  and sum these together plus the term of the general solution for the
  homogenous lin. recurrence to target the recurrence bases.
]

#lemma[Let $(b_n)_n, (d_n)_n$ be solutions of @non-hom.\
  Then $(b_n - d_n)$ is also a solution. This sequence also is homogonous.
]
#algorithm[non-hom. linear recurrence][
  1. To solve, find solution of hom. lin. recurrence $a^*_n$.
  2. Also find solutions for non-hom. lin. recurrences $tilde(a)^(1..s)_n)$
    for each summand of $g(n)$.
  Note that $sum^s_(i=1) tilde(a)^(s)_n$ is a solution for some recurrence bases.
  3. Find parameters $gamma_(i, j)$ of $a^*_n$ such that it meets the bases.
  4. The final solution has the form $a^*_n + sum^s_(i=1) tilde(a)^(s)_n$
    with the right parameters $gamma_(i, j)$
]

#definition[Catalan Number][
  The number of well-formed bracket expressions of length $n$ is
  $
    c_n = binom(2n, n) 1 / (n+1)
  $
]
