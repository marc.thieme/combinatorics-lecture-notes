#import "lib.typ": *

= Partitions
#proof-to-remember[
#theorem[
  $p_"dist" (n) = p_"odd" (n)$
]
#proof[
#let pdist = $p_"dist" (n)$
#let podd = $p_"odd" (n)$
  We describe a bijection from #pdist to #podd.

  Let $n = lambda_1 + ... + lambda k$.
  For all $i in [k]$, let $u_i, (a_i)$ such that $lambda_i = u_i 2^(a_i)$ with odd $u_i$.
  Since $u_i$ are odd, we can know construct an odd partition
  $
    n = 2^(a_1) dot u_1 + ... + 2^(a_k) dot u_k
  $. 

  We now look at the inverse of the function to show that it is in fact bijective.

  Let $n = c_1 dot u_1 + ... + c_k dot u_k$ where $u_k$ are odd.
  For every $i in [k]$, looking at the binary representation of $c_i$, let $b_(i j)$ be the index of the $j$th 1 in that representation of $c_i$.
  In other words, $c_i = sum_j 2^(b_(i j))$. 
  
  // Now we can reconstruct the sequence $ (a_i)_(i in [k]) = b_(1 1), b_(1 2), ..., b_(2 1), b_(2 2), ..., b_(k *) $.

  Now look at the partition $ m = sum_(i in [k]) [sum_j 2^(b_(i j)) u_i] $.
  We can first observe that the inner sum is always precisely $c_i$.
  As such, we see that $m = n$. 
  Furthermore, we see that for each summand which is divisible by $u_i$, the number of 2s in its prime factorisation is different.
  Therefore, all summands are pairwise distinct and it is a distinct partition of $n$.

  If we now apply our function to this partition, we see that we get back $n = c_1 dot u_1 + ... + c_k dot u_k$ because the inner sum collapses to $c_i u_i$.
  Therefore, we have proven that each distinct partition has a unique inverse odd partition and as such, our function is a bijection.
]
]

== Basics
#let stirling(n, k) = $vec(delim: "{", n, k)$
#definition[Stirling numbers][
  $
    s^(II)_(k) = stirling(n, k) = "# partitions of [n] into k non-empty parts"
  $
]
#definition[$B_n$][Bell Number][
  $B_n := sum_(a=0)^(n) stirling(n, k) = "# partitions of [n]"$
]
#interpretation[
  Let $n = p_1 dot ... dot p_n$ distinct primes.
  Then $B_n$ is the \# ways of writing $x$ as a product of integers.
]

#theorem[$B_n$][Identities][
  - $B_n = sum_(k=0)^(n-1) binom(n-1, k) B_k$
  - $B_n = 1 / e sum_(k=0)^infinity k^n / k!$
]

#definition[Crossing Sets][
  Sets $A$ and $B$ are corrsing if $
    exists_(i < j < k < l in [n]) : {i, k} subset.eq A, {j, l} subset.eq B
  $
]

#definition[Non-crossing Partition][
  We say that a partition $A_1, ..., A_k subset.eq [n]$ is crossing iff no
  two sets of the partition are crossing.
]

#theorem[][
  The \# of non-crossing partitions of $[n]$ is equal to the catalan nuber $c_n$.
]

== Ferrer's Diagrams
#definition[Partition of $n$][
  A partition of $n$ is an ordered tuple $(a_1, ..., a_s)$ with $a_1 + ... + a_s = n$
]

#principle[Ferrer's Diagram][
  Subgrid consisting of $s$ rows with $a_1, ..., a_s$ elements
  where the first row is $a_1$ long (the longest) and the rest are decending.
]

#definition[Conjugate Partition][
  Obtained when taking the transpose of the Ferrer's Diagram
]

#theorem[Euler][
  $ sum_(n=0)^infinity p(n) x^n = product_(k=1)^infinity 1 / (1-x^k) $
]

#definition[Common partition types][
  / $P_"odd"(n)$: Partition into odd numbers
  / $P_"dist"(n)$: Partition into distinct numbers
]

#theorem[Euler Product][
  $
    sum_(n=0)^infinity p(x) x^n = product_(k = 1)^infinity 1 / (1 - x^k)
  $
]
