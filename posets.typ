#import "lib.typ": *

= Partially Ordered Sets
#definition[$(X, epsilon subset.eq X times X)$][Poset][Partially ordered set][
  $epsilon$ is
  + *reflexive*: $(x, x) in epsilon quad forall_(x in X)$
  + *transitive* $(x, y), (y, z) in epsilon => (x, z) in epsilon$
  + *antisymmetric*: $(x <= y and y <= x) => x = y$
]

#notation[][
  - $not (x <= y) and not(y <= x) quad :<=> quad x || y$ incomparable.
  - if $x < y and not exists_z x < z < y quad :<=> quad x lt.dot y$ cover relation.
]

#definition[Hasse Diagram][
  Graph where the edges are the cover relations.
]

#example[Boolean Lattice][
  / Boolean Lattice: $(2^([n]), subset.eq)$
  #definition[Indicator Vector $arrow(i_A) = (a_1, ..., a_n)$ such that $a_i = cases(1 "if" i in A, 0 "else")$]
  Then $x lt.dot y$ means that the hamming distance between their indicator vectors is 1.
  \ This induces a hypercube.

  #principle[
    Finding layers in the boolean lattice \
    #zb In the boolean lattice, we can identify layers $binom([n], floor(n/2))$.
  ]
]

#definition[][
  / Chain: Elements of increasing order
  / Height: Size of the longest chain
  / Antichain: Set set where elements are pairwise incomparable
  / Width: Size of the largest antichain
  / Maximal & Minimal elements: obv.
]

#proof-to-remember[
#theorem[Antichain Partitioning][Mirsky's][
  Th elements of every poset $P = (x, <=)$ can be partitioned into $h(P)$ antichains
]
#proof[
  Take any poset $P = (x, <=)$ and any partition into antichains $S : union.big_S = P$.
  Let $C$ denote a largest chain in $P$.
  We first proof that there are no two elements $a, b in C$ such that they both lie in the same element of $S$.

  Take any two $a, b in C$. Since $C$ is a chain, $a parallel.not b$. 
  Therefore, there exists antichain in $P$ which can contain both $a$ and $b$.
  Since all elements in $S$ are antichains, no element in $S$ contains both $a$ and $b$.

  Consequently, $|S| >= |C|$.
  Therefore, all antichain partitions require at least $|A| = h(P)$ elements.

  The second part of the statement, we prove inductively over the height of a poset $n := h(P)$.
  #base Let $P$ be a single-element-poset. $h(P) = 1$ and $P$ can be partitioned into 1 antichain.
  #assume Every poset $P$ of height $n => h(P)$ can be partitioned into $n$ antichains.
  #step[n] Take a poset $P$ of height $n+1$.
  Let $M := min(P)$ the set of minimum elements of $P$.
  Note that $M$ is an antichain as otherwise, two elements of $M$ would compare.
  Also note that each maximal chain in $P$ intersects $M$.

  Therefore, $P' = P without M$ is a poset with height $h(P') = h(P) - 1 = n$.
  As such, there exists an antichain partion of $P'$ with $n$ elements.
  Adding $M$ to this partition, we receive an antichain partition of $P$ of $n+1 = h(P)$ elements.
]
]
#theorem[Antichain Partitioning][Dilworth's][
  Th elements of every poset $P = (x, subset.eq)$ can be partitioned into $omega(P)$ chains.
]

#definition[Induced Subposet][
  $P=(X, <=_P)$ is the subposet of $Q=(X, <=_Q)$ induced by $X subset.eq Y$ iff $
    x_1 <=_P x_2 <=> x_1 <=_Q x_2
$
]

#definition[Subposet][
  $P$ (with set $X$) is a subposet of Q (with set $Y$) ($P subset.eq Q$ iff $
    exists_("injective" f : X -> Y) forall_(x_1, x_2 in X) : (x_1 <=_P x_2 <=> f(x_1) <= f(x_2))
  $
]

#definition[Extension][
  $Q  = (X, <=_Q)$ is an extension of $P = (X, subset.eq_P)$ (! _same groundset_ !) iff $
    x_1 <=_P x_2 => x_1 <=_Q x_2
  $
  Or written alternatively iff $<=_P subset.eq <=_Q$
]

// TODO a lot of stuff

#proof-to-remember[
#theorem[Sperner's][
  $w(B_n) = binom(n, floor(n/2))$
]
#proof[
  We say an element of the boolean lattice $S$ meets a permutation of $[n]$ $P$ if the set of the first $|S|$ elements
  of $P$ is exactly $S$. 
  In other words, a permutation $P$ meets all sets of its first numbers.

  Take the largest antichain $A$ in the boolean lattice.
  Since no two elements in $A$ have subset relations, no pair of elements can meet the same permutation.
  Otherwise, we immediately see that the shorter set would be subset of the larger set because it meets a prefix of the second
  permutation's meet. 
  If the two sets have the same size and were different, they could also not be subset-related.
  We also see that for a set $U = binom([n], k)$, it is met by $k!(n-k)!$ permutations since there are $k!$
  possibilities to arrange its own elements as a prefix and $(n-k)!$ arrangements of the residual elements as the suffix.

  Since we cannot meet more than $n!$ permutations of length $n$ we can formulate:
  $
    n! &>= sum_(a in A) |a|!(n-|a|)! >= sum_(a in A) floor(n/2) ! (n-floor(n/2))! >= |A| floor(n/2)! ceil(n/2)!
    \ <=> (n!)/(floor(n/2)!ceil(n/2)!) &= binom(n, floor(n/2)) >= |A|
  $

  Which proves the upper bound. 

  We observe the lower bound by looking at the set $A:=binom([n], floor(n/2))$ of size $binom(n, floor(n/2))$.
  Since each distinct set $A$ has the same size, no two sets are subset-related.
  Thereby, $A$ is an antichain, which constructively proves the lower bound.
]
]

#definition[in $B_n$][Full Chain][
  A *Full Chain* is a chian in $B_n$ iwth $n+1$ elements.
  $ zb quad vec({1, ..., n}, dots.v, {1, 2}, {1}, emptyset, delim: #none) $
]
#(
  slim.theorem
)[ which sits at the top.][Each full chain corresponds to one permutation of $[n]$]

#definition[Symmetric Chain][
  A chain in $B_n$ is symmetric if it has elements of sizes $k, k+1, ..., $
  //TODO
]

#(
  theorem
)[$B_n$ Symmetric Chain Decomposition][Every $B_n$ is decomposable into symmetric chains]

#notation[$a plus.circle b$][
  The union of two incomparable chians of length $a$ and $b$.
]

#theorem[2-dimensionality][
  A poset $P$ is 2-dimensional if there is a linear extension $L$ of $P$ ordering all copies of $1 plus.circle 2$ in $P$.

  That is a linear extension $L$ of $P$ s.t. for any copy of ${a, b, c}, {b <= c}$, either $b <= c <= a$ or $a <= b <= c$
]

#definition[$P$][$n$-dimensionality][
  $P$ is $d$-dimensional if it is a subposet of $(RR^d, <=_"dom")$
]

#theorem[$n$-dimensionality][
  $P$ is $d$-dimensional iff $d$ is the smallest number of linear extensions of $P$ whose intersection in $P$.

]

#(slim.theorem)[$dim(P) <= omega(P)$][Dimensionality buond]

#proof-to-remember[
#theorem[5.8][$P = (x, subset.eq)$][Dimensionality Dominance][$
    dim(P) <= d <==>\
     exists_(f: X -> RR^d "injective") : x subset.eq y <=> f(x) <= f(y)
$]
#proof[
  We first show that if such an injective function exists, according linear extensions exist as well.

  Let $f : X -> RR^d$ be injective such that $x <= y <=> f(x) <= f(y)$.
  Let $L_i quad (i in [d])$ order the elements $x in X$ according to the $i$th coordinate of $f(x)$.
  Denote $L = sect_(i in [d]) L_i$

  Now take $x, y in X$ with $x subset.eq y$.
  Then $f(x) <= f(y)$ and therefore for all $i in [d]$, $f(x)_i <=_"dom" f(y)_i$ and therefore $x <=_L_i y$.
  As such $x <=_L y$.

  Take $x, y in X$ with $x || y$.
  In this case, $f(x) ||_"dom" f(y)$ and vice versa.
  Therefore, there exist coordinate $i$ and $j$ such that $f(x)_i <= f(y)_i$ but $f(y)_j <= f(x)_j$.
  Consequently, $x <=_L_i y$ but $y <=_L_j x$ and therefore $x ||_L y$.

  Therefore, $L$ is isomorph to $P$.


  For the other direction, take a list of linear extensions $L_1, ..., L_d$ such that $sect L_i$ is isomorph of $P$.
  For any $x in P$, let $x_L_i$ denote the index of $x$ in the ordering of $L_i$.
  We now construct $f : P -> RR^d$ with $x |-> product_(i in [d]) x_L_i e_i$ where $e_i$ is the $i$th unit vector.

  Now take $x, y in X$ with $x subset.eq y$.
  For all $L_i$, $x <=_L_i y$.
  Therefore, over all coordinates $i$, $f(x)_i <= f(y)_i$ and therefore $f(x) <=_"dom" f(y)$.

  Take $x, y in X$ with $x || y$.
  Then there exist $i, j in [d]$ such that $x <=_L_i y$ but $y <=_L_i x$.
  Therefore, $f(x)_i <= f(y)_j$ but $f(y)_j <= f(x)_j$ and as such, $f(x) ||_"dom" f(y))$.

  Therefore, we have proven that $f$ according to the theorem exists.
]
]

#definition[of $x in P$][Poset P][Rank][The Rank of $P$ is the size of the largest chain beneath $x$.]

#definition[Ranked Poset][
  A Poset is ranked if all maximal chains ending in an element have the same size.
]

#definition[Unrefinable][
  A chain is unrefinable if we cannot insert an element in between elements of the chain.
]

#let rank = "rank"

#definition[Symmetric][
  An unrefinable chain is called *Symmetric* iff $
    rank(min(C)) + rank(max(C)) = rank(P) + 1
  $
]

== Lattices
#definition[$P=(X, <=)$][Meet][
  $z = x or y$ iff $x, y <= z$ and $
    forall_(z' in X) x, y <= z' => z <= z'
  $
]

== Extremal Numbers for Posets
#let La = "La"
#definition[$La(n, H)$][
  $La(n, H) := max{
      |X|: X subset.eq 2^([n]), X "does not contain P as a subposet"
    }$
]

#(slim.definition)[$La^*(n, P) := max {
      |X| : X subset.eq 2^([n]), X supset.eq_"ind" P
    }$][$La^*(n, P)$]
